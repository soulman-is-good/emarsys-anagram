const readline = require('readline');

const isOfSameChars = str =>
  typeof str === 'string' && str.length > 1
    ? str.split('').reduce((result, char) => result && char === str[0], true)
    : false;
const sortWord = str =>
  typeof str === 'string' && str
    ? str
        .split('')
        .sort()
        .join('')
    : '';

const buildAnagramMap = input =>
  new Promise(resolve => {
    const lines = readline.createInterface({ input });
    const tree = {};

    lines.on('line', line => {
      const anagram = line.toLowerCase().replace(/\s+/g, '');

      if (anagram.length < 2 || isOfSameChars(anagram)) {
        return;
      }
      const sorted = sortWord(anagram);

      if (!tree[sorted]) {
        tree[sorted] = [];
      }

      if (!tree[sorted].includes(line)) {
        tree[sorted].push(line);
      }
    });
    lines.on('close', () => resolve(tree));
  });

module.exports = {
  buildAnagramMap,
  sortWord,
  isOfSameChars,
};
