const fs = require('fs');
const os = require('os');
const readline = require('readline');
const chalk = require('chalk');
const { buildAnagramMap, sortWord, isOfSameChars } = require('./utils');

const QUESTION = `
=================================
Enter a word to find anagram:
=================================
> `;

function stdOut(message) {
  process.stdout.write(`${message}${os.EOL}`);
}

function wantQuit(rl) {
  return new Promise(resolve => {
    rl.question('Do you want to quit? (y/n) ', answer => {
      const yes = answer.toLowerCase().replace(/\s+/g, '');

      if (yes[0] === 'y') {
        resolve(true);
      } else if (yes[0] === 'n') {
        resolve(false);
      } else {
        stdOut('Please type y or n');
        wantQuit(rl).then(resolve);
      }
    });
  });
}

function questionWord(rl, map) {
  rl.question(chalk.gray(QUESTION), answer => {
    const word = answer.toLowerCase().replace(/\s+/g, '');

    if (!word) {
      wantQuit(rl).then(yes => {
        if (yes) {
          rl.close();
        } else {
          questionWord(rl, map);
        }
      });

      return;
    }

    if (word.length === 1 || isOfSameChars(word)) {
      stdOut(chalk.magenta('This word has no anagrams'));
      questionWord(rl, map);

      return;
    }
    const sortedWord = sortWord(word);
    const words =
      map[sortedWord] &&
      Array.from(map[sortedWord]).filter(item => item.toLowerCase() !== word);

    if (words && words.length > 0) {
      const output = chalk.cyan(words.join(os.EOL));

      stdOut(output);
    } else {
      stdOut(chalk.magenta('No anagrams found for this input. Try again...'));
    }
    questionWord(rl, map);
  });
}

async function run(anagramFile) {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  const rs = fs.createReadStream(anagramFile);

  stdOut('Please wait, preparing anagrams...');
  const map = await buildAnagramMap(rs);

  questionWord(rl, map);
}

module.exports = { run };
