const { PassThrough } = require('stream');
const { assert } = require('chai');
const { buildAnagramMap, sortWord, isOfSameChars } = require('../src/utils');

const createMockStream = (data = []) => {
  const mockStream = new PassThrough();

  setTimeout(() => {
    mockStream.emit('data', `${data.join('\n')}\n`);
    mockStream.end();
  });

  return mockStream;
};

describe('Utils tests', () => {
  describe('#isOfSameChars', () => {
    it('should return false for', () => {
      assert.isFalse(isOfSameChars('1'));
      assert.isFalse(isOfSameChars(''));
    });
    it('should return true for strings with same char', () => {
      assert.isTrue(isOfSameChars('111'));
      assert.isTrue(isOfSameChars('aaaa'));
      assert.isTrue(isOfSameChars('$$'));
    });
    it('should return false for strings without same char', () => {
      assert.isFalse(isOfSameChars('111112'));
      assert.isFalse(isOfSameChars('2321'));
      assert.isFalse(isOfSameChars('aba'));
    });
  });

  describe('#sortWord', () => {
    it('should be accessible', () => {
      assert.equal(sortWord(), '');
      assert.equal(sortWord(''), '');
      assert.equal(sortWord(123), '');
      assert.equal(sortWord(null), '');
      assert.equal(sortWord(true), '');
    });
    it('should sort word', () => {
      const result = sortWord('test');

      assert.equal(result, 'estt');
    });
  });

  describe('#buildAnagramMap', () => {
    it('should be accessible', async () => {
      const tree = await buildAnagramMap(createMockStream());

      assert.deepEqual(tree, {});
    });

    it('should build anagram map', async () => {
      const wordList = ['nag a ram', 'mangara'];
      const sortedWord = sortWord('anagram');
      const tree = await buildAnagramMap(createMockStream(wordList));

      assert.deepEqual(Array.from(tree[sortedWord]), wordList);
    });
  });
});
