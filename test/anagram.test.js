const path = require('path');
const { spawn } = require('child_process');
const readline = require('readline');
const { assert } = require('chai');

const anagramCli = path.resolve(__dirname, '../bin/anagram.js');
const delay = timeout => new Promise(resolve => setTimeout(resolve, timeout));
const runAnagramCli = () => {
  const instance = spawn(anagramCli, {
    env: { ...process.env, NODE_ENV: 'test' },
  });
  const rl = readline.createInterface({
    input: instance.stdout,
  });
  const lines = [];

  instance.stdin.setEncoding('utf8');
  instance.stdout.setEncoding('utf8');
  instance.stderr.setEncoding('utf8');

  rl.on('line', line => lines.push(line));

  return {
    instance,
    async getLine(skip = 0) {
      let i = 0;

      while (i < 10 && lines.length <= skip) {
        await delay(100);
        i += 1;
      }
      i = skip;
      let result = lines.shift();

      while (i > 0) {
        result = lines.shift();
        i -= 1;
      }

      return result;
    },
    onExit() {
      return new Promise(resolve => instance.on('exit', resolve));
    },
    kill(signal) {
      instance.kill(signal);

      return this.onExit();
    },
    write(data) {
      return new Promise(resolve => instance.stdin.write(data, resolve));
    },
    get writable() {
      return instance.stdin.writable;
    },
  };
};

describe('CLI functional tests', () => {
  it('should start and exit with SIGINT', async () => {
    const cli = runAnagramCli();

    assert.equal(await cli.getLine(), 'Please wait, preparing anagrams...');
    assert.equal(await cli.getLine(2), 'Enter a word to find anagram:');

    await cli.kill('SIGINT');

    assert.isFalse(cli.writable);
  });

  it('should lookup for anagram', async () => {
    const cli = runAnagramCli();

    await cli.getLine(4);
    await cli.write('anagram\n');
    const out = [];

    out.push(await cli.getLine());
    out.push(await cli.getLine());

    assert.equal(out.join('\n'), '> nag a ram\nmangara');

    await cli.kill('SIGINT');

    assert.isFalse(cli.writable);
  });

  it('should ask and quit on empty sumbmission', async () => {
    const cli = runAnagramCli();

    await cli.getLine(4);
    await cli.write('\n\n');
    const out = await cli.getLine();

    assert.equal(out, '> Do you want to quit? (y/n) Please type y or n');
    await cli.write('y\n');
    await cli.onExit();

    assert.isFalse(cli.writable);
  });

  it('should continue if answered n on exit', async () => {
    const cli = runAnagramCli();

    await cli.getLine(4);
    await cli.write('\n\n');
    await cli.getLine();
    await cli.write('n\n');
    const out = await cli.getLine(2);

    assert.equal(out, 'Enter a word to find anagram:');
    assert.isTrue(cli.writable);
    cli.kill('SIGTERM');
  });
});
