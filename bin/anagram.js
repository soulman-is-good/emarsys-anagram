#!/usr/bin/env node
const path = require('path');
const { run } = require('../src/index');

const ANAGRAM_FILE =
  process.env.NODE_ENV === 'test'
    ? path.resolve(__dirname, '../test/words.txt')
    : path.resolve(__dirname, '../wordlist.txt');

run(ANAGRAM_FILE);
